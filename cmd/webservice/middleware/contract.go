package middleware

import (
	"julo-backend/internal/usecase"
	"net/http"
)

type middleware struct {
	usecase usecase.Usecase
}

type Middleware interface {
	MustAuthenticate(next http.Handler) http.Handler
	MustWalletEnabled(next http.Handler) http.Handler
	MustWalletDisabled(next http.Handler) http.Handler
}

func NewMiddleware(usecase usecase.Usecase) Middleware {
	return &middleware{
		usecase: usecase,
	}
}
