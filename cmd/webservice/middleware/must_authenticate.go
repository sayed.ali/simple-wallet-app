package middleware

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	"net/http"
	"strings"
)

func (m *middleware) MustAuthenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		if token == "" {
			httputil.WriteErrorResponse(w, &base.ErrorResponse{
				Error: map[string]interface{}{
					"token": []string{"Authorization token is required"},
				},
			})
			return
		}
		if !strings.HasPrefix(token, "Token ") {
			httputil.WriteErrorResponse(w, &base.ErrorResponse{
				Error: map[string]interface{}{
					"token": []string{"Authorization token should start with 'Token ...'"},
				},
			})
			return

		}

		sessionId := strings.ReplaceAll(token, "Token ", "")

		sessionInfo, err := m.usecase.Authenticate(r.Context(), sessionId)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: map[string]interface{}{
					"authentication": []string{err.Error()},
				},
			})
			return

		}

		ctx := context.WithValue(r.Context(), iconstant.CustomerXIDKey, sessionInfo.CustomerXID)
		ctx = context.WithValue(ctx, iconstant.WalletIDKey, sessionInfo.WalletID)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
