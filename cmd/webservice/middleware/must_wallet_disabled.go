package middleware

import (
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	pkgerror "julo-backend/pkg/errors"
	"net/http"
)

func (m *middleware) MustWalletDisabled(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		walletInfo, err := m.usecase.GetWalletInfo(r.Context())
		if err != nil {
			httputil.WriteErrorResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return
		}

		if walletInfo.Status == iconstant.WalletStatusEnabled {
			httputil.WriteErrorResponse(w, &base.ErrorResponse{
				Error: pkgerror.ErrWalletAlreadyEnabled.Error(),
			})
			return
		}
		next.ServeHTTP(w, r)
	})
}
