package handlers

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	"net/http"
)

type GetWalletTransactionsUsecaseFn func(ctx context.Context) ([]*dto.TransactionInfo, error)

func GetWalletTransactions(fn GetWalletTransactionsUsecaseFn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		transactionInfo, err := fn(r.Context())
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return
		}

		httputil.WriteSuccessResponse(w, iconstant.EntityResponse{
			iconstant.TransactionEntityResponse: transactionInfo,
		})
	}
}
