package handlers

import (
	"julo-backend/internal/utils/httputil"
	"net/http"
)

func HealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		httputil.WriteSuccessResponse(w, nil)
	}
}
