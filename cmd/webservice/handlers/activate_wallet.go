package handlers

import (
	"context"
	"julo-backend/internal/constant"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	"net/http"
)

type ActivateWalletUsecaseFn func(ctx context.Context) (*dto.WalletInfo, error)

func ActivateWallet(fn ActivateWalletUsecaseFn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		walletInfo, err := fn(r.Context())
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return
		}

		httputil.WriteSuccessResponse(w, constant.EntityResponse{
			constant.WalletEntityResponse: walletInfo,
		})
	}
}
