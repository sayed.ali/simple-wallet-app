package handlers

import (
	"context"
	"julo-backend/internal/constant"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	pkgerror "julo-backend/pkg/errors"
	"net/http"
)

type DepositToWalletUsecaseFn func(ctx context.Context, referrenceID string, amount int64) (*dto.TransactionInfo, error)

func DepositToWallet(fn DepositToWalletUsecaseFn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		amount, err := httputil.ExtractAmount(r)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: pkgerror.ErrorBody{
					"amount": []string{err.Error()},
				},
			})
			return
		}

		referrenceID, err := httputil.ExtractReferrenceID(r)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: pkgerror.ErrorBody{
					"amount": []string{err.Error()},
				},
			})
			return

		}

		depositInfo, err := fn(r.Context(), referrenceID, amount)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return
		}

		httputil.WriteSuccessResponse(w, constant.EntityResponse{
			constant.DepositEntityResponse: depositInfo,
		})
	}
}
