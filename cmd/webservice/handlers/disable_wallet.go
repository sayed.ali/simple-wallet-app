package handlers

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	pkgerror "julo-backend/pkg/errors"
	"net/http"
)

type DisableWalletUsecaseFn func(ctx context.Context, isDisabled bool) (*dto.WalletInfo, error)

func DisableWallet(fn DisableWalletUsecaseFn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		isDisabeld, err := httputil.ExtractIsDisabled(r)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: pkgerror.ErrorBody{
					"is_disabled": []string{err.Error()},
				},
			})
			return
		}

		walletInfo, err := fn(r.Context(), isDisabeld)
		if err != nil {
			httputil.WriteErrorResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return
		}

		httputil.WriteSuccessResponse(w, iconstant.EntityResponse{
			iconstant.WalletEntityResponse: walletInfo,
		})
	}
}
