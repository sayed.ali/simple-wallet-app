package handlers

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/utils/httputil"
	"julo-backend/pkg/dto/base"
	pkgerror "julo-backend/pkg/errors"
	"net/http"
)

type InitAccountUsecaseFn func(ctx context.Context, walletID string) (string, error)

func InitAccount(fn InitAccountUsecaseFn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		customerID := r.FormValue("customer_xid")
		if customerID == "" {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: pkgerror.ErrorBody{
					"customer_xid": []string{
						pkgerror.ErrFieldRequired.Error(),
					},
				},
			})
			return
		}

		token, err := fn(r.Context(), customerID)
		if err != nil {
			httputil.WriteFailResponse(w, &base.ErrorResponse{
				Error: err.Error(),
			})
			return

		}

		httputil.WriteSuccessResponse(w, iconstant.EntityResponse{
			"token": token,
		})
	}
}
