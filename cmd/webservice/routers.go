package webservice

import (
	"julo-backend/cmd/webservice/handlers"
	"julo-backend/cmd/webservice/middleware"
	"julo-backend/internal/usecase"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func initRouter(usecase usecase.Usecase) http.Handler {

	router := mux.NewRouter().StrictSlash(true)
	m := middleware.NewMiddleware(usecase)
	apiV1 := router.PathPrefix("/api/v1").Subrouter()

	apiV1.HandleFunc("/healthcheck", handlers.HealthCheck()).Methods(http.MethodGet)
	apiV1.HandleFunc("/init", handlers.InitAccount(usecase.InitAccount)).Methods(http.MethodPost)

	apiV1Secure := apiV1.NewRoute().Subrouter()
	apiV1Secure.Use(m.MustAuthenticate)
	{
		apiV1MustWalletDisabled := apiV1Secure.NewRoute().Subrouter()
		apiV1MustWalletDisabled.Use(m.MustWalletDisabled)
		apiV1MustWalletDisabled.HandleFunc("/wallet", handlers.ActivateWallet(usecase.EnableWallet)).Methods(http.MethodPost)
	}

	apiV1SecuredWalletEnabled := apiV1Secure.NewRoute().Subrouter()
	{
		apiV1SecuredWalletEnabled.Use(m.MustWalletEnabled)
		apiV1SecuredWalletEnabled.HandleFunc("/wallet", handlers.GetWalletInfo(usecase.GetWalletInfo)).Methods(http.MethodGet)
		apiV1SecuredWalletEnabled.HandleFunc("/wallet", handlers.DisableWallet(usecase.DisableWallet)).Methods(http.MethodPatch)
		apiV1SecuredWalletEnabled.HandleFunc("/wallet/transactions", handlers.GetWalletTransactions(usecase.GetWalletTransactions)).Methods(http.MethodGet)
		apiV1SecuredWalletEnabled.HandleFunc("/wallet/deposits", handlers.DepositToWallet(usecase.CreateDeposit)).Methods(http.MethodPost)
		apiV1SecuredWalletEnabled.HandleFunc("/wallet/withdrawals", handlers.WithdrawFromWallet(usecase.CreateWithdraw)).Methods(http.MethodPost)
	}

	corslib := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})

	return corslib.Handler(router)

}
