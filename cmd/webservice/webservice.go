package webservice

import (
	"context"
	"errors"
	"fmt"
	"julo-backend/internal/usecase"
	"julo-backend/internal/utils/configutil"
	"log"
	"net/http"
	"time"
)

var server *http.Server

func Start() func() {
	usecase := usecase.InitUsecase()
	config := configutil.Get()

	server = &http.Server{
		Addr:    fmt.Sprintf(":%s", config.Server.Port),
		Handler: initRouter(usecase),
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			if !errors.Is(err, http.ErrServerClosed) {
				log.Fatalf("error while starting http server. err %v", err)
			}
		}
	}()

	log.Printf("http server start on port %s", config.Server.Port)

	return func() {
		gracefulStop()
	}
}

func gracefulStop() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}

	log.Println("Server shutdown gracefully")

}
