package httputil

import (
	"errors"
	"julo-backend/internal/constant"
	pkgerror "julo-backend/pkg/errors"
	"net/http"
	"strconv"
)

func ExtractReferrenceID(r *http.Request) (string, error) {
	referrenceID := r.FormValue(string(constant.FormReferrenceID))
	if referrenceID == "" {
		return "", pkgerror.ErrFieldRequired
	}

	return referrenceID, nil

}

func ExtractAmount(r *http.Request) (int64, error) {

	amountStr := r.FormValue(string(constant.FormAmount))
	if amountStr == "" {
		return 0, pkgerror.ErrFieldRequired
	}

	amount, err := strconv.ParseInt(amountStr, 10, 64)
	if err != nil {
		return 0, errors.New("must be a non-negative number")
	}

	return amount, nil
}

func ExtractIsDisabled(r *http.Request) (bool, error) {
	isDisabledStr := r.FormValue(string(constant.FormIsDisabled))
	if isDisabledStr == "" {
		return false, pkgerror.ErrFieldRequired
	}

	isDisabled, err := strconv.ParseBool(isDisabledStr)
	if err != nil {
		return false, errors.New("must be true or false")
	}

	return isDisabled, nil
}
