package httputil

import (
	"encoding/json"
	"julo-backend/pkg/constant"
	"julo-backend/pkg/dto/base"
	"net/http"
)

func writeResponse(w http.ResponseWriter, resp *base.Response) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	payload := encodeResponse(resp)
	w.Write(payload)
}

func WriteSuccessResponse(w http.ResponseWriter, data interface{}) {
	baseResponse := &base.Response{
		Status: string(constant.ResponseSuccess),
		Data:   data,
	}

	writeResponse(w, baseResponse)
}

func WriteFailResponse(w http.ResponseWriter, error *base.ErrorResponse) {
	baseResponse := &base.Response{
		Status: string(constant.ResponseFail),
		Data:   error,
	}

	writeResponse(w, baseResponse)

}

func WriteErrorResponse(w http.ResponseWriter, error *base.ErrorResponse) {
	baseResponse := &base.Response{
		Status: string(constant.ResponseError),
		Data:   error,
	}

	writeResponse(w, baseResponse)
}

func encodeResponse(data interface{}) []byte {
	bytesJson, _ := json.Marshal(data)
	return bytesJson
}
