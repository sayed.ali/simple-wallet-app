package configutil

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

const configFile = "./config/config.yaml"

type redisConfig struct {
	Host string `yaml:"host"`
}

type databaseConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
	Name string `yaml:"name"`
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

type serverConfig struct {
	Port string `yaml:"port"`
}

type config struct {
	Server   serverConfig   `yaml:"server"`
	Redis    redisConfig    `yaml:"redis"`
	Database databaseConfig `yaml:"database"`
}

var appConfig *config

func LoadConfig() error {
	workDir, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("error load working directory. err %v", err)
	}
	configFile, err := ioutil.ReadFile(path.Join(workDir, configFile))
	if err != nil {
		return fmt.Errorf("error reading config file. err %v", err)
	}

	// to expand configuration from env vars
	configFile = []byte(os.ExpandEnv(string(configFile)))

	if err := yaml.Unmarshal(configFile, &appConfig); err != nil {
		return fmt.Errorf("error unmharsal yaml config. err %v", err)
	}

	return nil
}

func Get() *config {
	return appConfig
}
