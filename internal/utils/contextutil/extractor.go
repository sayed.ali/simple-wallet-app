package contextutil

import (
	"context"
	"julo-backend/internal/constant"
	pkgerror "julo-backend/pkg/errors"
)

func ExtractWalletID(ctx context.Context) (string, error) {
	walletID, exist := ctx.Value(constant.WalletIDKey).(string)
	if !exist {
		return "", pkgerror.ErrFieldRequired
	}
	return walletID, nil
}

func ExtractWalletOwnerID(ctx context.Context) (string, error) {
	ownerID, exist := ctx.Value(constant.CustomerXIDKey).(string)
	if !exist {
		return "", pkgerror.ErrFieldRequired
	}
	return ownerID, nil

}
