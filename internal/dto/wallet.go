package dto

import "julo-backend/internal/constant"

type WalletInfo struct {
	ID         string                `json:"id"`
	OwnedBy    string                `json:"owned_by"`
	Status     constant.WalletStatus `json:"status"`
	Balance    int64                 `json:"balance"`
	EnabledAt  *string               `json:"enabled_at,omitempty"`
	DisabledAt *string               `json:"disabled_at,omitempty"`
}
