package dto

type TransactionInfo struct {
	ID           string  `json:"id"`
	Status       string  `json:"status"`
	TransactedAt *string `json:"transacted_at,omitempty"`
	Type         string  `json:"type"`
	Amount       int64   `json:"amount"`
	ReferrenceID string  `json:"referrence_id"`

	//Optional info
	DepositedBy  *string `json:"deposited_by,omitempty"`
	DepositedAt  *string `json:"deposited_at,omitempty"`
	WithrdrawnBy *string `json:"withdrawn_by,omitempty"`
	WithrdrawnAt *string `json:"withdrawn_at,omitempty"`
}

func (t *TransactionInfo) ExcludeTransactionTime() *TransactionInfo {
	t.TransactedAt = nil
	return t
}

func (t *TransactionInfo) WithDepositInfo(depositerID string) *TransactionInfo {
	t.DepositedBy = &depositerID
	t.DepositedAt = t.TransactedAt
	return t
}

func (t *TransactionInfo) WithWithdrawInfo(withdrawerID string) *TransactionInfo {
	t.WithrdrawnBy = &withdrawerID
	t.WithrdrawnBy = t.TransactedAt
	return t
}
