package dto

type SessionInfo struct {
	WalletID    string
	CustomerXID string
}
