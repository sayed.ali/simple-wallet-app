package model

import "julo-backend/internal/dto"

type Transaction struct {
	ID           string
	Status       string
	TransactedAt *string
	Type         string
	Amount       int64
	ReferrenceID string
}

func (t *Transaction) GetAll() []interface{} {
	return []interface{}{
		&t.ID,
		&t.Status,
		&t.TransactedAt,
		&t.Type,
		&t.Amount,
		&t.ReferrenceID,
	}
}

func (t *Transaction) ToDto() *dto.TransactionInfo {
	return &dto.TransactionInfo{
		ID:           t.ID,
		Status:       t.Status,
		TransactedAt: t.TransactedAt,
		Type:         t.Type,
		Amount:       t.Amount,
		ReferrenceID: t.ReferrenceID,
	}
}
