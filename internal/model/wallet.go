package model

import (
	"julo-backend/internal/constant"
	"julo-backend/internal/dto"
)

type Wallet struct {
	ID         string
	OwnedBy    string
	Status     string
	EnabledAt  *string
	DisabledAt *string
	Balance    int64
}

func (w *Wallet) GetAll() []interface{} {
	return []interface{}{
		&w.ID,
		&w.OwnedBy,
		&w.Status,
		&w.EnabledAt,
		&w.DisabledAt,
		&w.Balance,
	}
}

func (w *Wallet) ToHashmapFormat() map[string]interface{} {
	return map[string]interface{}{
		"wallet_id":     w.ID,
		"wallet_status": w.Status,
		"customer_xid":  w.OwnedBy,
	}
}

func (w *Wallet) ToDto() *dto.WalletInfo {
	return &dto.WalletInfo{
		ID:         w.ID,
		OwnedBy:    w.OwnedBy,
		Status:     constant.WalletStatus(w.Status),
		EnabledAt:  w.EnabledAt,
		DisabledAt: w.DisabledAt,
		Balance:    w.Balance,
	}
}
