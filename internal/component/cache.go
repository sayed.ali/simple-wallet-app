package component

import (
	"context"
	"julo-backend/internal/utils/configutil"
	"log"

	"github.com/go-redis/redis/v8"
)

type Cache struct {
	Master *redis.Client
}

func InitCache() *Cache {
	cache := &Cache{}
	config := configutil.Get()

	cache.Master = redis.NewClient(&redis.Options{
		Addr: config.Redis.Host,
		DB:   0,
	})

	pingResult := cache.Master.Ping(context.Background())
	if pingResult.Err() != nil {
		log.Fatalf("error testing redis connection. err %v", pingResult.Err())
	}

	return cache
}
