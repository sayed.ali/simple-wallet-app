package component

import (
	"database/sql"
	"fmt"
	"julo-backend/internal/utils/configutil"
	"log"
	"time"

	"github.com/go-sql-driver/mysql"
)

type Database struct {
	Master *sql.DB
	Slave  *sql.DB
}

func InitDatabase() *Database {

	db := &Database{}
	config := configutil.Get()
	var err error

	dbMasterConfig := &mysql.Config{
		User:                 config.Database.User,
		Passwd:               config.Database.Pass,
		DBName:               config.Database.Name,
		Net:                  "tcp",
		Addr:                 fmt.Sprintf("%s:%s", config.Database.Host, config.Database.Port),
		AllowNativePasswords: true,
	}

	if db.Master, err = sql.Open("mysql", dbMasterConfig.FormatDSN()); err != nil {
		log.Fatalf("error open database connection. err %v", err)
	}

	if err = db.Master.Ping(); err != nil {
		log.Fatalf("error trying database opened connection. err %v", err)
	}

	db.Master.SetConnMaxIdleTime(30 * time.Second)
	db.Master.SetMaxOpenConns(10)

	return db
}
