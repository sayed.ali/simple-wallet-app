package repository

import (
	"context"
	"julo-backend/internal/model"
)

func (r *repository) StoreSession(ctx context.Context, sessionID string, walletInfo *model.Wallet) error {
	_, err := r.cache.Master.HSet(ctx, sessionID, walletInfo.ToHashmapFormat()).Result()
	if err != nil {
		return err
	}
	return nil
}
