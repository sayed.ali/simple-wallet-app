package repository

import (
	"context"
	"julo-backend/internal/constant"
	"julo-backend/internal/utils/contextutil"
)

func (r *repository) UpdateWalletStatus(ctx context.Context, tx DBTransaction, status constant.WalletStatus) error {
	walletID, err := contextutil.ExtractWalletID(ctx)
	if err != nil {
		return err
	}
	query := `
		UPDATE wallets set 
			status = ?,
			enabled_at = (CASE WHEN status = ? THEN NOW() END),
			disabled_at = (CASE WHEN status = ? THEN NOW() END)
		WHERE 
			id = ?
	`
	if _, err := tx.ExecContext(ctx, query, status, constant.WalletStatusEnabled, constant.WalletStatusDisabled, walletID); err != nil {
		return err
	}

	return nil
}
