package repository

import (
	"context"
	"julo-backend/internal/model"
	"julo-backend/internal/utils/contextutil"
)

func (r *repository) GetWalletInfo(ctx context.Context, tx DBTransaction) (*model.Wallet, error) {
	walletID, err := contextutil.ExtractWalletID(ctx)
	if err != nil {
		return nil, err
	}
	wallet := &model.Wallet{}
	query := "SELECT id, owned_by, status, enabled_at, disabled_at, balance from wallets where id = ?"
	if err := tx.QueryRowContext(ctx, query, walletID).Scan(wallet.GetAll()...); err != nil {

		return nil, err
	}

	return wallet, nil
}
