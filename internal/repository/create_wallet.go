package repository

import (
	"context"
	"errors"
	"julo-backend/internal/constant"
	"log"
)

func (r *repository) CreateWallet(ctx context.Context, tx DBTransaction, walletID, accountID string) error {
	query := "INSERT INTO wallets (id, owned_by) values(?, ?)"
	if _, err := tx.ExecContext(ctx, query, walletID, accountID); err != nil {
		if errors.As(err, &mysqlError) && mysqlError.Number == constant.ErrSqlDuplicateKey {
			return errors.New("account already created")
		} else {
			log.Printf("error db. err %v", err)
			return errors.New("failed to create account")
		}

	}

	return nil
}
