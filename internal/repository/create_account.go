package repository

import (
	"context"
	"errors"
	"julo-backend/internal/constant"

	"github.com/go-faker/faker/v4"
)

func (r *repository) CreateAccount(ctx context.Context, tx DBTransaction, accountID string) error {
	query := "INSERT INTO accounts (id, name) values(?, ?)"
	_, err := tx.ExecContext(ctx, query, accountID, faker.Name())

	if errors.As(err, &mysqlError) && mysqlError.Number == constant.ErrSqlDuplicateKey {
		return errors.New("account already created")
	}

	return nil
}
