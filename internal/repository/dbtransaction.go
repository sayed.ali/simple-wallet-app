package repository

import "database/sql"

func (r *repository) Begin() (*sql.Tx, error) {
	return r.db.Master.Begin()
}

func (r *repository) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (r *repository) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
