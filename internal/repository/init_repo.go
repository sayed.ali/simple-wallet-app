package repository

import (
	"database/sql"
	"julo-backend/internal/component"

	"github.com/go-sql-driver/mysql"
)

var mysqlError *mysql.MySQLError

type repository struct {
	*sql.Tx
	db    *component.Database
	cache *component.Cache
}

func InitRepository() Repository {
	return &repository{
		db:    component.InitDatabase(),
		cache: component.InitCache(),
	}
}
