package repository

import (
	"context"
	"julo-backend/internal/utils/contextutil"
)

func (r *repository) UpdateWalletBalance(ctx context.Context, tx DBTransaction, balance int64) error {
	walletID, err := contextutil.ExtractWalletID(ctx)
	if err != nil {
		return err
	}
	query := `
		UPDATE wallets set 
			balance = balance + ?
		WHERE 
			id = ?
	`
	if _, err = tx.ExecContext(ctx, query, balance, walletID); err != nil {
		return err
	}

	return nil
}
