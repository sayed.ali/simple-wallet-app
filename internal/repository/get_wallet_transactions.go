package repository

import (
	"context"
	"errors"
	"julo-backend/internal/model"
	"julo-backend/internal/utils/contextutil"
	"log"
)

func (r *repository) GetWalletTransactions(ctx context.Context, tx DBTransaction) ([]*model.Transaction, error) {
	walletID, err := contextutil.ExtractWalletID(ctx)
	if err != nil {
		return nil, err
	}
	transactions := []*model.Transaction{}

	query := `SELECT id, status, transacted_at, type, amount, referrence_id FROM transactions WHERE wallet_id = ? ORDER by transacted_at DESC`

	rows, err := tx.QueryContext(ctx, query, walletID)
	if err != nil {
		log.Printf("error query db. err %v", err)
		return nil, errors.New("failed to get transaction data")
	}

	defer rows.Close()

	for rows.Next() {
		transaction := &model.Transaction{}
		if err := rows.Scan(transaction.GetAll()...); err != nil {
			return nil, errors.New("failed mapping result to persistent model")
		}

		transactions = append(transactions, transaction)
	}

	return transactions, nil

}

func (r *repository) GetWalletTransactionByID(ctx context.Context, tx DBTransaction, trxID string) (*model.Transaction, error) {
	transaction := &model.Transaction{}

	query := `SELECT id, status, transacted_at, type, amount, referrence_id FROM transactions WHERE id = ?`

	err := tx.QueryRowContext(ctx, query, trxID).Scan(transaction.GetAll()...)
	if err != nil {
		log.Printf("error query db. err %v", err)
		return nil, errors.New("failed to get transaction data")
	}

	return transaction, nil

}
