package repository

import (
	"context"
	"errors"
	"julo-backend/internal/constant"
	"julo-backend/internal/utils/contextutil"
	pkgerror "julo-backend/pkg/errors"
	"log"

	"github.com/google/uuid"
)

func (r *repository) CreateWithdraw(ctx context.Context, tx DBTransaction, referrenceID string, amount int64, status constant.TransactionStatus) (string, error) {
	transactionID := uuid.NewString()
	walletID, err := contextutil.ExtractWalletID(ctx)
	if err != nil {
		return "", err
	}

	query := "INSERT INTO transactions (id, wallet_id, referrence_id, amount, type, status) VALUES (?, ?, ?, ?, ?, ?)"

	if _, err := tx.ExecContext(ctx, query, transactionID, walletID, referrenceID, amount,
		constant.TransactionWithdraw, status); err != nil {
		if errors.As(err, &mysqlError) && mysqlError.Number == constant.ErrSqlDuplicateKey {
			return "", pkgerror.ErrDuplicateReferrence
		} else {
			log.Printf("error db. err %v", err)
			return "", errors.New("failed to insert data to db")
		}
	}

	return transactionID, nil
}
