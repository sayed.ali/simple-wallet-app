package repository

import (
	"context"
)

func (r *repository) GetSessionInfo(ctx context.Context, sessionID string) (map[string]string, error) {
	sessionInfo, err := r.cache.Master.HGetAll(ctx, sessionID).Result()
	if err != nil {
		return nil, err
	}

	return sessionInfo, nil

}
