package repository

import (
	"context"
	"database/sql"
	"julo-backend/internal/constant"
	"julo-backend/internal/model"
)

type DBTransaction interface {
	QueryRow(query string, args ...interface{}) *sql.Row
	Query(query string, args ...interface{}) (*sql.Rows, error)
	Exec(query string, args ...interface{}) (sql.Result, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
}

type Repository interface {
	Begin() (*sql.Tx, error)
	Commit(tx *sql.Tx) error
	Rollback(tx *sql.Tx) error

	//Base DB Methods
	//Database repo methods
	CreateAccount(ctx context.Context, tx DBTransaction, customerID string) error
	CreateWallet(ctx context.Context, tx DBTransaction, walletID, customerID string) error
	GetWalletInfo(ctx context.Context, tx DBTransaction) (*model.Wallet, error)
	UpdateWalletStatus(ctx context.Context, tx DBTransaction, status constant.WalletStatus) error
	GetWalletTransactions(ctx context.Context, tx DBTransaction) ([]*model.Transaction, error)
	GetWalletTransactionByID(ctx context.Context, tx DBTransaction, trxID string) (*model.Transaction, error)
	CreateDeposit(ctx context.Context, tx DBTransaction, referrenceID string, amount int64) (string, error)
	CreateWithdraw(ctx context.Context, tx DBTransaction, referrenceID string, amount int64, status constant.TransactionStatus) (string, error)
	UpdateWalletBalance(ctx context.Context, tx DBTransaction, balance int64) error

	//Cache repo methods
	StoreSession(ctx context.Context, sessionID string, walletInfo *model.Wallet) error
	GetSessionInfo(ctx context.Context, sessionID string) (map[string]string, error)
}
