package usecase

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/dto"
)

func (u *usecase) EnableWallet(ctx context.Context) (*dto.WalletInfo, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	u.repo.UpdateWalletStatus(ctx, tx, iconstant.WalletStatusEnabled)
	walletInfo, err := u.repo.GetWalletInfo(ctx, tx)
	if err != nil {
		return nil, err
	}
	tx.Commit()

	return walletInfo.ToDto(), nil

}
