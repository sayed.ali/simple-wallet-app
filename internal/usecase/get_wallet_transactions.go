package usecase

import (
	"context"
	"julo-backend/internal/dto"
)

func (u *usecase) GetWalletTransactions(ctx context.Context) (trxInfoDto []*dto.TransactionInfo, err error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	transactionInfo, err := u.repo.GetWalletTransactions(ctx, tx)
	if err != nil {
		return nil, err
	}

	for _, transaction := range transactionInfo {
		trxInfoDto = append(trxInfoDto, transaction.ToDto())
	}

	tx.Commit()

	return trxInfoDto, nil

}
