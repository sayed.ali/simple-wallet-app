package usecase

import (
	"context"
	"crypto/sha1"
	"fmt"
	"strconv"
	"time"

	"github.com/google/uuid"

	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/model"
)

func (u *usecase) InitAccount(ctx context.Context, customerID string) (string, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return "", err
	}
	defer tx.Rollback()

	if err := u.repo.CreateAccount(ctx, tx, customerID); err != nil {
		return "", err
	}

	newGeneratedWalletID := uuid.NewString()

	err = u.repo.CreateWallet(ctx, tx, newGeneratedWalletID, customerID)
	if err != nil {
		return "", err
	}

	token := u.generateToken()

	err = u.repo.StoreSession(ctx, token, &model.Wallet{
		ID:      newGeneratedWalletID,
		OwnedBy: customerID,
		Status:  string(iconstant.WalletStatusDisabled),
	})

	if err != nil {
		return "", err
	}

	tx.Commit()

	return token, nil
}

func (u *usecase) generateToken() string {

	sha := sha1.New()
	unixStr := strconv.FormatInt(time.Now().Unix(), 10)
	sha.Write([]byte(unixStr))

	return fmt.Sprintf("%x", sha.Sum(nil))
}
