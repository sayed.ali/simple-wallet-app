package usecase

import (
	"context"
	iconstant "julo-backend/internal/constant"
	"julo-backend/internal/dto"
)

func (u *usecase) DisableWallet(ctx context.Context, isDisable bool) (*dto.WalletInfo, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	if err := u.repo.UpdateWalletStatus(ctx, tx, iconstant.WalletStatusDisabled); err != nil {
		return nil, err
	}

	walletInfo, err := u.repo.GetWalletInfo(ctx, tx)
	if err != nil {
		return nil, err
	}

	tx.Commit()

	return walletInfo.ToDto(), nil

}
