package usecase

import (
	"context"
	"errors"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/contextutil"
	"log"
)

func (u *usecase) CreateDeposit(ctx context.Context, referrenceID string, amount int64) (*dto.TransactionInfo, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	transactionID, err := u.repo.CreateDeposit(ctx, tx, referrenceID, amount)
	if err != nil {
		return nil, err
	}

	if err := u.repo.UpdateWalletBalance(ctx, tx, amount); err != nil {
		log.Println(err)
		return nil, errors.New("failed to update wallet balance")
	}

	transactionInfo, err := u.repo.GetWalletTransactionByID(ctx, tx, transactionID)
	if err != nil {
		return nil, errors.New("failed to get saved transaction")
	}

	tx.Commit()

	walletOwner, err := contextutil.ExtractWalletOwnerID(ctx)
	if err != nil {
		return nil, err
	}

	return transactionInfo.ToDto().
		WithDepositInfo(walletOwner).
		ExcludeTransactionTime(), nil

}
