package usecase

import (
	"context"
	"julo-backend/internal/dto"
)

type Usecase interface {
	InitAccount(ctx context.Context, customerID string) (string, error)
	Authenticate(ctx context.Context, sessionID string) (*dto.SessionInfo, error)
	GetWalletInfo(ctx context.Context) (*dto.WalletInfo, error)
	EnableWallet(ctx context.Context) (*dto.WalletInfo, error)
	DisableWallet(ctx context.Context, isDisabeld bool) (*dto.WalletInfo, error)
	GetWalletTransactions(ctx context.Context) ([]*dto.TransactionInfo, error)
	CreateDeposit(ctx context.Context, referrenceID string, amount int64) (*dto.TransactionInfo, error)
	CreateWithdraw(ctx context.Context, referrenceID string, amount int64) (*dto.TransactionInfo, error)
}
