package usecase

import (
	"context"
	"errors"
	"julo-backend/internal/constant"
	"julo-backend/internal/dto"
	"julo-backend/internal/utils/contextutil"
)

func (u *usecase) CreateWithdraw(ctx context.Context, referrenceID string, amount int64) (*dto.TransactionInfo, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	walletInfo, err := u.repo.GetWalletInfo(ctx, tx)
	if err != nil {
		return nil, err
	}

	var (
		transactionID string
	)

	if amount > walletInfo.Balance {
		if transactionID, err = u.repo.CreateWithdraw(ctx, tx, referrenceID, amount, constant.TransactionFailed); err != nil {
			return nil, err
		}
	} else {
		if transactionID, err = u.repo.CreateWithdraw(ctx, tx, referrenceID, amount, constant.TransactionSuccess); err != nil {
			return nil, err
		}
		if err := u.repo.UpdateWalletBalance(ctx, tx, (amount * -1)); err != nil {
			return nil, errors.New("failed to update wallet balance")
		}
	}
	transactionInfo, err := u.repo.GetWalletTransactionByID(ctx, tx, transactionID)
	if err != nil {
		return nil, errors.New("failed to get saved transaction")
	}

	tx.Commit()

	walletOwner, err := contextutil.ExtractWalletOwnerID(ctx)
	if err != nil {
		return nil, err
	}

	return transactionInfo.ToDto().
		WithWithdrawInfo(walletOwner).
		ExcludeTransactionTime(), nil

}
