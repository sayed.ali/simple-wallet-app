package usecase

import (
	"context"
	"errors"
	"julo-backend/internal/dto"
)

func (u *usecase) Authenticate(ctx context.Context, sessionID string) (*dto.SessionInfo, error) {
	sessionInfo, err := u.repo.GetSessionInfo(ctx, sessionID)
	if err != nil {
		return nil, err
	}

	if len(sessionInfo) == 0 {
		return nil, errors.New("authentication failed")
	}

	return &dto.SessionInfo{
		WalletID:    sessionInfo["wallet_id"],
		CustomerXID: sessionInfo["customer_xid"],
	}, nil
}
