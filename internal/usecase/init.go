package usecase

import "julo-backend/internal/repository"

type usecase struct {
	repo repository.Repository
}

func InitUsecase() Usecase {
	repo := repository.InitRepository()
	return &usecase{
		repo: repo,
	}
}
