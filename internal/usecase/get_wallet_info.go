package usecase

import (
	"context"
	"julo-backend/internal/dto"
)

func (u *usecase) GetWalletInfo(ctx context.Context) (*dto.WalletInfo, error) {
	tx, err := u.repo.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	walletInfo, err := u.repo.GetWalletInfo(ctx, tx)
	if err != nil {
		return nil, err
	}

	tx.Commit()

	return walletInfo.ToDto(), nil

}
