package constant

type TransactionStatus string
type TransactionType string

const (
	TransactionSuccess TransactionStatus = "success"
	TransactionFailed  TransactionStatus = "failed"

	TransactionDeposit  TransactionType = "deposit"
	TransactionWithdraw TransactionType = "withdrawal"
)

const (
	TransactionEntityResponse = "transactions"
	DepositEntityResponse     = "deposit"
	WithdrawEntityResponse    = "withdrawal"
)
