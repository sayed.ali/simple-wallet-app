package constant

type WalletStatus string
type ContextKey string

const (
	WalletStatusEnabled  WalletStatus = "enabled"
	WalletStatusDisabled WalletStatus = "disabled"
)

const (
	WalletStatusKey ContextKey = "wallet_status"
	WalletIDKey     ContextKey = "wallet_id"
	CustomerXIDKey  ContextKey = "customer_xid"
)

const (
	WalletEntityResponse = "wallet"
)
