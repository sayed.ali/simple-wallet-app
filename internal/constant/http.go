package constant

type FormField string
type EntityResponse map[string]interface{}

const (
	FormAmount       FormField = "amount"
	FormReferrenceID FormField = "referrence_id"
	FormCustomerXID  FormField = "customer_xid"
	FormIsDisabled   FormField = "is_disabled"
)
