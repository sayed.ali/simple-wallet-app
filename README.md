TODO
1. I will add docker-compose for local testing to ensure that there are no cross-platform incompatibilities. (Done)
2. I will add environment variable loader for app start (Done)
3. I will perform a small refactors to bad smell codes (Partially Done)
4. the most important, i will implement database transaction for request command (Done)
5. unit test (Too bad, time ran out)


## Summary
1. This app has a few drawbacks, such as simple authentication with no encryption, and only store the most important information for every required request such as walletID and customerXID
2. For init account, let assume the user will only have single wallet account, so i put the user data and wallet data simultaneusly
3. Since this app is made in a simple way, I'm not implement golang multithreading as this is unnecessary


## Step By Step installation

1. make sure you haved installed and ready go-migrate tool, or you could refer to [this site](https://github.com/golang-migrate/migrate/blob/master/cmd/migrate/README.md)
2. you are welcome to choose to run this app via docker, (I've presented docker-compose here)
3. or you choose to run locally, make sure mysql and redis were running on your host


### Running With Docker

1. first step, run this command
```bash
    docker-compose up --build -d
```
2. I've made a port forward for mysql server to `6606`, so before we start, please run the migration using this port
    to run migration, you could simply run: 
    ```bash
    # for database username and password, please refer to ./docker/mysql/entrypoint.sql

    export DB_AUTH=julo_simple_wallet_rw:some_random_password
    export DB_URI=localhost:6606
    make migrate
    ```
3. you could follow this [original documentation](https://documenter.getpostman.com/view/8411283/SVfMSqA3?version=latest#99bca41f-ecf6-4dee-a44d-154d2f8f4096) to test the code, as i've set up nginx as a reverse proxy to forward any traffic from port 80 to port 8080 in backend service

### Running in Local (not recommended)
1. make sure you have run mysql and redis on your local
2. run this command
* Setup environment variables
```bash
   export SERVER_PORT=8080
   export DB_HOST=localhost
   export DB_PORT=3306
   export DB_USER=#fill in your db user
   export DB_PASS=#fill in your db pass
   export DB_NAME=julo_simple_wallet
   export REDIS_HOST=localhost:6379

   export DB_AUTH=${DB_USER}:${DB_PASS}
   export DB_URI=${DB_HOST}:${DB_PORT}
```
* run db entrypoint (given you dont have the db, it will be error when run db migration)
```bash
    mysql -u [your db user] -p < ./docker/mysql/entrypoint.sql
```
* after this, run db migration 
```bash
    make migrate
```
* finally, run the app, i hope it won't be an issue
```bash
    go run main.go
```

3. since you run this app on local, you should use http port 8080 for testing
