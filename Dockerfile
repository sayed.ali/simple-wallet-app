FROM golang:1.20-alpine as builder

ENV APP_DIR /usr/src/app/julo-backend

RUN mkdir -p ${APP_DIR}
COPY . ${APP_DIR}

RUN cd ${APP_DIR} && CGO_ENABLED=0 go build --ldflags '-w -d -s'

FROM alpine:latest

ENV APP_DIR /usr/src/app/julo-backend

WORKDIR ${APP_DIR}

COPY --from=builder ${APP_DIR} .

RUN apk add curl tzdata