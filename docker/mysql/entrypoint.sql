CREATE DATABASE `julo_simple_wallet`;

CREATE USER 'julo_simple_wallet_rw'@'%' IDENTIFIED WITH mysql_native_password BY 'some_random_password';

GRANT ALL PRIVILEGES ON `julo_simple_wallet`.* TO 'julo_simple_wallet_rw'@'%';

FLUSH PRIVILEGES;