package errors

import "errors"

type ErrorBody map[string][]string

var (
	ErrAccountAlreadyCreated = errors.New("account already created")
	ErrFieldRequired         = errors.New("missing data for required field")
	ErrWalletAlreadyEnabled  = errors.New("wallet already enabled")
	ErrWalletDisabled        = errors.New("wallet is disabled")
	ErrInsufficientBalance   = errors.New("insufficient balance")
	ErrDuplicateReferrence   = errors.New("referrence id is duplicated")
)
