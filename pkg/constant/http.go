package constant

type ResponseStatus string

const (
	ResponseSuccess ResponseStatus = "success"
	ResponseFail    ResponseStatus = "fail"
	ResponseError   ResponseStatus = "error"
)
