CREATE TABLE wallets (
    `id` VARCHAR(36) NOT NULL,
    `owned_by` VARCHAR(36) NOT NULL,
    `status` ENUM('enabled', 'disabled') DEFAULT 'disabled',
    `enabled_at` DATETIME DEFAULT NULL,
    `disabled_at` DATETIME DEFAULT NULL,
    `balance` BIGINT(20) DEFAULT '0',
    PRIMARY KEY (`id`, `owned_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci