CREATE TABLE transactions (
    `id` VARCHAR(36) NOT NULL,
    `wallet_id` VARCHAR(36) NOT NULL,
    `status` ENUM('success', 'failed') NOT NULL,
    `transacted_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `type` ENUM('deposit', 'withdrawal') NOT NULL,
    `amount` BIGINT(20) NOT NULL,
    `referrence_id` VARCHAR(36) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`wallet_id`, `referrence_id`, `type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
