package main

import (
	"julo-backend/cmd/webservice"
	"julo-backend/internal/utils/configutil"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if err := configutil.LoadConfig(); err != nil {
		log.Fatalf("error while load config. err %v", err)
	}

	stopFunc := webservice.Start()

	sigInt := make(chan os.Signal, 1)
	signal.Notify(sigInt, syscall.SIGINT, syscall.SIGTERM)

	<-sigInt
	stopFunc()
	log.Println("Application stopped")
	os.Exit(0)

}
